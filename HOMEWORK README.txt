In D:\homework-six-level-two\include\spring\Framework\IApplicationModel.h

Changed:
std::map<const std::string, const boost::any> m_TransientData;
To:
std::map<const std::string, boost::any> m_TransientData;

And

Changed:
std::map<const std::string, const boost::any> getTransientData() const
To:
std::map<const std::string, boost::any> getTransientData() const

In D:\homework-six-level-two\include\spring\Framework\SceneMachine.h

Changed:
CSceneMachine(std::map<const std::string, IScene*> a_SceneCollection, std::map<const std::string, const boost::any> a_TransientData, std::unique_ptr<QMainWindow>& av_xMainWindow, const std::string ac_sInitialScene);
To:
CSceneMachine(std::map<const std::string, IScene*> a_SceneCollection, std::map<const std::string, boost::any> a_TransientData, std::unique_ptr<QMainWindow>& av_xMainWindow, const std::string ac_sInitialScene);

And

Changed:
std::map<const std::string, const boost::any> m_TransientDataCollection;
To:
std::map<const std::string, boost::any> m_TransientDataCollection;

In D:\homework-six-level-two\src\spring\Framework\SceneMachine.cpp

Changed:
CSceneMachine::CSceneMachine(std::map<const std::string, IScene*> a_SceneCollection, std::map<const std::string, const boost::any> a_TransientData, std::unique_ptr<QMainWindow>& av_xMainWindow, const std::string ac_sInitialScene)
To:
CSceneMachine::CSceneMachine(std::map<const std::string, IScene*> a_SceneCollection, std::map<const std::string, boost::any> a_TransientData, std::unique_ptr<QMainWindow>& av_xMainWindow, const std::string ac_sInitialScene)

In D:\homework-six-level-two\include\spring\Framework\IScene.h

Changed:
std::map<const std::string, const boost::any> m_TransientDataCollection;
To:
std::map<const std::string, boost::any> m_TransientDataCollection;

And

Changed:
virtual std::map<const std::string, const boost::any> uGetTransientData()
To:
virtual std::map<const std::string, boost::any> uGetTransientData()

And

Changed:
virtual void setTransientData(std::map<const std::string, const boost::any>& ac_xTransientData)
To:
virtual void setTransientData(std::map<const std::string, boost::any>& ac_xTransientData)

Done! :) Now you can use the = operator instead of erase and emplace methods.


