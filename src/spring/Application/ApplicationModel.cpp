#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>

const std::string initialSceneName = "InitialScene";
const std::string baseSceneName = "BaseScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{
	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);

		IScene* baseScene = new BaseScene(baseSceneName);
		m_Scenes.emplace(baseSceneName, baseScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		//add initial values for all transient data 

		std::string applicationName = "Undefined";
		m_TransientData.emplace("ApplicationName", applicationName);

		unsigned int sampleRate = 25600;
		m_TransientData.emplace("SampleRate", sampleRate);

		double displayTime = 0.1;
		m_TransientData.emplace("DisplayTime", displayTime);

		unsigned int refreshRate = 1;
		m_TransientData.emplace("RefreshRate", refreshRate);
	}
}
